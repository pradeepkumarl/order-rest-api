package com.synechron.orderrestapi.service;

import com.synechron.orderrestapi.model.Order;
import com.synechron.orderrestapi.repository.OrderRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@Service
@RequiredArgsConstructor
public class OrderService {

    private final OrderRepository orderRepository;

    @Transactional
    public Order saveOrder(Order order){
        return this.orderRepository.save(order);
    }

    @Transactional
    public Map<String, Object> fetchAll(int pageNo, int size, String order){
        PageRequest pageRequest = PageRequest.of(pageNo, size, Sort.by(order));
        final Page<Order> pageResponse = this.orderRepository.findAll(pageRequest);
        final long totalElements = pageResponse.getTotalElements();
        final int totalPages = pageResponse.getTotalPages();
        final List<Order> orders = pageResponse.getContent();
        Map<String, Object> response = new LinkedHashMap<>();
        response.put("total", totalElements);
        response.put("pages", totalPages);
        response.put("data", orders);
        return response;
    }

    @Transactional
    public Order findOrderById(long orderId){
        return this.orderRepository.findById(orderId).orElseThrow(() -> new IllegalArgumentException("Invalid Order Id"));
    }

    @Transactional
    public void deleteOrderById(long orderId){
        this.orderRepository.deleteById(orderId);
    }
}