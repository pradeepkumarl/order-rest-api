package com.synechron.orderrestapi.service;

import com.synechron.orderrestapi.model.Customer;
import com.synechron.orderrestapi.repository.CustomerRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import java.time.LocalDate;
import java.util.LinkedHashMap;
import java.util.Map;

@Service
@RequiredArgsConstructor
public class CustomerService {

    private final CustomerRepository customerRepository;

    public Map< String, Object> findAllByDOBAfter(LocalDate dob, int records, int page){
        final PageRequest pageRequest = PageRequest.of(page, records);
        final Page<Customer> customersResponse = this.customerRepository.findCustomerByDobAfter(dob, pageRequest);
        Map<String, Object> response = new LinkedHashMap<>();
        response.put("pages", customersResponse.getTotalPages());
        response.put("records", customersResponse.getTotalElements());
        response.put("data", customersResponse.getContent());
        return response;
    }
}