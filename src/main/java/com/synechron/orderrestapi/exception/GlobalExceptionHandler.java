package com.synechron.orderrestapi.exception;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@Component
@RestControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler(IllegalArgumentException.class)
    public ResponseEntity<Error> handleInvalidOrderId(Exception exception){
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new Error(100, exception.getMessage()));
    }
}

@Data
@NoArgsConstructor
@AllArgsConstructor
class Error{
    private int code;
    private String message;
}