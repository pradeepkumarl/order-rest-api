package com.synechron.orderrestapi.repository;

import com.synechron.orderrestapi.model.Customer;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import java.time.LocalDate;
import java.util.List;

@Repository
public interface CustomerRepository extends JpaRepository<Customer, Long> {

    Page<Customer> findCustomerByDobAfter(LocalDate startDate, Pageable page);

    List<Customer> findCustomerByDobBetween(LocalDate startDate, LocalDate endDate);

    List<Customer> findCustomersByEmailAddresIsContaining(String domainName);

    List<Customer> findByNameLikeOrderByNameAsc(String name);
}