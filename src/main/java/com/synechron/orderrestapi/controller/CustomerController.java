package com.synechron.orderrestapi.controller;

import com.synechron.orderrestapi.service.CustomerService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDate;
import java.util.Map;

@RestController
@RequestMapping("/api/v1/customers")
@RequiredArgsConstructor
@Slf4j
public class CustomerController {

    private final CustomerService customerService;

    @GetMapping
    public Map<String, Object> findCustomersByDOBAfter(
                @RequestParam(value = "dob", required = false, defaultValue = "1991-10-10") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate startDate,
                @RequestParam(value = "records", defaultValue = "10", required = false) int records,
                @RequestParam(value = "page", defaultValue = "0", required = false) int page
                ){
        log.info("Accepted the Local date in the format :: {}", startDate instanceof LocalDate);
        final Map< String, Object> response = this.customerService.findAllByDOBAfter(startDate, records, page);
        log.info("Count :: "+response.size());
        return response;
    }
}